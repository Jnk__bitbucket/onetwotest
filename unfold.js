const direction = (function* () {
  let i = 0;
  const direction = ["left", "down", "right", "up"];
  while (true) {
    yield direction[i++ % direction.length];
  }
})();

export default arr => {
  const center = Math.floor(arr.length / 2);
  let x = center;
  let y = center;
  let r = [];

  const getItems = ({ value: direction }, count) => {
    while (count--) {
      switch (direction) {
        case "left":
          y = y - 1;
          break;
        case "down":
          x = x + 1;
          break;
        case "right":
          y = y + 1;
          break;
        case "up":
          x = x - 1;
          break;
        default:
          break;
      }
      r.push(arr[x][y]);
    }
  };

  arr.forEach((_, index) => {
    if (index === 0) return;
    let line = 2;
    while (line--) {
      getItems(direction.next(), index);
    }
  });

  const [_, ...last] = arr[0].reverse();
  return [arr[center][center], ...r, ...last].join(",");
};
