import unfold from "./unfold";

const makeMatrix = n => {
  const len = n * 2 - 1;
  let r = [];
  for (let i = 0; i < len; i++) {
    let line = [];
    for (let j = 0; j < len; j++) {
      line[j] = Math.floor(1 + Math.random() * 9);
    }
    r.push(line);
  }
  return r;
};

const matrix = makeMatrix(2);
console.log("матрица:");
console.log(matrix);
console.log("ответ:");
console.log(unfold(matrix));
